const LINK_SELECTOR = "h2>a";

function debounce(func, wait) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(context, args), wait);
    };
}

function escapeRegExp(string) {
    string = string.replace(/[.+?^${}()|[\]\\\/]/g, '\\$&');
    string = string.replaceAll("*", '.*');
    return string;
}

function matches(match, toBeMatched) {
    return match && toBeMatched.match("^" + escapeRegExp(match) + "$");
}

function getSearchResults() {
    return document.querySelectorAll("article");
}

function updateHiddenLinksNum(filteredResults) {

    document.getElementById("ddgfilter-num")?.remove();

    if (filteredResults && filteredResults.length > 0) {
        var linksNode = document.getElementsByClassName("react-results--main")[0];
        var numNode = document.createElement("div");
        numNode.textContent = "DDG-FILTER filtered " + filteredResults.length + " results";
        numNode.style.paddingLeft = '10px';
        numNode.style.paddingBottom = '5px';
        numNode.id = "ddgfilter-num";
        numNode.style.fontWeight = "bold";
        numNode.style.color = "var(--theme-col-txt-title)";
        var truncatedLinks = filteredResults.map(result => {
            var maxLengthAllowed = 120;
            if(result.length > maxLengthAllowed) {
                result = result.substring(0, maxLengthAllowed) + " ...";
            }
            return result;
        });
        numNode.title = truncatedLinks.join("\n");

        linksNode.prepend(numNode);
    }
}

function excludeSearchResult(element, excludeAll){
    return async (e) => {

        e.stopPropagation();
        var link = element.querySelectorAll(LINK_SELECTOR)[0].href;
        if(excludeAll) {
            link = "*://" + new URL(link).hostname + "*";
        }
        var result = await browser.storage.sync.get('urls');
        var urls = [link].concat(result.urls ?? []);
        browser.storage.sync.set({
            urls: urls
        });
    }
}

function addHideButtons() {

    [...document.getElementsByTagName("article")].forEach((element) => {

        var icon = document.createElement("img");
        icon.src = browser.runtime.getURL("icons/icon32.png");
        icon.width = "18";
        icon.classList.add("ddg-filter-icon");

        var buttonsDiv = document.createElement("div");
        buttonsDiv.classList.add("ddg-filter-buttons-div");
        
        var excludeButton = document.createElement("button");
        excludeButton.textContent = "Exclude";
        excludeButton.onclick = excludeSearchResult(element, false);
        excludeButton.classList.add("ddg-filter-button");

        var excludeDomainButton = document.createElement("button");
        excludeDomainButton.textContent = "Exclude domain";
        excludeDomainButton.onclick = excludeSearchResult(element, true);
        excludeDomainButton.classList.add("ddg-filter-button");

        buttonsDiv.appendChild(icon);
        buttonsDiv.appendChild(excludeButton);
        buttonsDiv.appendChild(excludeDomainButton);
        
        element.appendChild(buttonsDiv);
    });

}


function handleSearchResults(searchResults, filteredUrls, isActive, isEveryExcludeButtonHidden) {

    if (searchResults && searchResults.length > 0) {

        [...document.getElementsByClassName("ddg-filter")]?.forEach((element) => element.classList.remove("ddg-filter"));
        [...document.getElementsByClassName("ddg-filter-buttons-div")]?.forEach(element => element.remove());

        if(isEveryExcludeButtonHidden === false) {
            addHideButtons();
        }

        if (isActive) {
            var filteredResults = [];
            searchResults.forEach(element => {
                const a = element.querySelectorAll(LINK_SELECTOR)[0];
                const hrefMatchesAnyUrl = filteredUrls.reduce((result, curr) => result || matches(curr, a.href), false);
                if (hrefMatchesAnyUrl) {
                    filteredResults.push(a.href);
                    element.classList.add("ddg-filter");
                }
            });
        }

        updateHiddenLinksNum(filteredResults);
    }
}


function observeChanges(filteredUrls, isActive, isEveryExcludeButtonHidden) {

    var observer = new MutationObserver(debounce((mutations, me) => {
        
        observer.disconnect()

        const searchResults = getSearchResults();
        handleSearchResults(searchResults, filteredUrls, isActive, isEveryExcludeButtonHidden)
        
        observer.observe(document, {childList: true, subtree: true});
        
    }, 100));

    observer.observe(document, {
        childList: true,
        subtree: true
    });

    return observer;
}

(async () => {

    document.addEventListener("DOMContentLoaded", function (event) {
        var link = document.createElement("link");
        link.href = browser.runtime.getURL("css/main.css")
        link.rel = 'stylesheet'
        link.type = "text/css";
        document.head.appendChild(link);
    });

    var result = await browser.storage.sync.get('urls');
    var filteredUrls = result.urls ?? [];

    result = await browser.storage.sync.get('isActive');
    var isActive = result.isActive ?? true;

    result = await browser.storage.sync.get('isEveryExcludeButtonHidden');
    var isEveryExcludeButtonHidden = result.isEveryExcludeButtonHidden ?? false;

    var observer = observeChanges(filteredUrls, isActive, isEveryExcludeButtonHidden);

    handleSearchResults(getSearchResults(), filteredUrls, isActive);

    browser.storage.onChanged.addListener((changes, area) => {

        filteredUrls = changes.urls ? changes.urls.newValue : filteredUrls;
        isActive = changes.isActive ? changes.isActive.newValue : isActive;
        isEveryExcludeButtonHidden = changes.isEveryExcludeButtonHidden 
            ? changes.isEveryExcludeButtonHidden.newValue : isEveryExcludeButtonHidden;

        observer.disconnect();

        handleSearchResults(getSearchResults(), filteredUrls, isActive, isEveryExcludeButtonHidden);

        observer = observeChanges(filteredUrls, isActive, isEveryExcludeButtonHidden);
    });

})();