async function initTextArea() {

    var result = await browser.storage.sync.get('urls');
    var urls = result.urls ?? [];

    if (urls && urls.length > 0) {
        document.getElementById("urls-textarea").value = urls.join("\n");
    }
}

async function initCheckbox() {

    var result = await browser.storage.sync.get('isActive');
    var isActive = result.isActive ?? true;
    
    result = await browser.storage.sync.get('isEveryExcludeButtonHidden');
    var isEveryExcludeButtonHidden = result.isEveryExcludeButtonHidden ?? false;

    document.getElementById("active-checkbox").checked = isActive === true;
    document.getElementById("buttons-checkbox").checked = isEveryExcludeButtonHidden === true;
}

function addEventListeners() {

    document.getElementById("active-checkbox")
        .addEventListener('change', (event) => {
            browser.storage.sync.set({
                isActive: event.currentTarget.checked
            });

        });

    document.getElementById("buttons-checkbox")
        .addEventListener('change', (event) => {
            browser.storage.sync.set({
                isEveryExcludeButtonHidden: event.currentTarget.checked
            });

        });

    document.getElementById("save-button")
        .addEventListener("click", e => {
            var text = document.getElementById("urls-textarea").value;
            if (text) {
                var urls = text.split("\n");
                browser.storage.sync.set({
                    urls: urls
                });
            }
        });
}

(async () => {

    await initTextArea();

    await initCheckbox();

    addEventListeners();

})();